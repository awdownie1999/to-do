@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Edit Task</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="POST" action="{{ route('tasks.update', $task->id) }}">
                    @method('PATCH')
                    @csrf
                    <br /><br /><br />
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $task->name }}"><br /><br />

                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="datetime-local" name="date" class="form-control" value="{{ $task->date }}"><br />

                    </div>
                    <div class="form-group">
                        <label for="complete">Complete</label>
                        <input type="checkbox" name="complete" class="form-control" value="{{ $task->complete }}"><br />
                    </div>

                    <div class="form-group">
                        <label for="type_id">Task Type</label>
                        <select name="type_id">
                            @foreach ($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-secondary">Update</button>
                </form>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
