@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Tasks</h1>
            
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <br /><br />
                @if ($tasks == null)
                    <h1>Please Login</h1>
                @else
                    <div><a class="btn btn-primary" href=" {{ route('tasks.create') }}">Create New Task</a></div>
                    <br/><br/>
                    <div id='calendar'></div>
                    <br /><br />
                    {{$tasks}}
                    <table>
                        <thead>
                            <tr>
                                <th>ID&nbsp;&nbsp;</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Complete</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($tasks as $task)
                                <tr class="tasks">
                                    <td class="task-id">{{ $task->id }}&nbsp;&nbsp;</td>
                                    <td class="task-name">{{ $task->name }}&nbsp;&nbsp;</td>
                                    <td class="task-type">{{ $task->type->name }}&nbsp;&nbsp;</td>
                                    <td class="task-date">{{ $task->date }}&nbsp;&nbsp;</td>
                                    @if ($task->complete == false)
                                        <td>False</td>
                                    @else
                                        <td>True</td>
                                    @endif

                                    <td><a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-primary">Edit</a>
                                    </td>

                                    <td>
                                        <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                    <div>
                        {{ $tasks->links() }}
                    </div>
                    

                @endif

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
