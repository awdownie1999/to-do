@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Edit Types</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="POST" action="{{ route('types.update', $type->id) }}">
                    @method('PATCH')
                    @csrf
                    <br /><br /><br />
                    <div>
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $type->name }}"><br /><br />

                    </div>
                    <button type="submit" class="btn btn-secondary">Update</button>
                </form>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
