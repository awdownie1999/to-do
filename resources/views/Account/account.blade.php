@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Account</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <br />
                <br />
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left;">
                            <ul style="list-style-type: none;">

                                <li>{{ __('Name') }} : {{ Auth::user()->name }}</li>
                                <li>{{ __('Email') }} : {{ Auth::user()->email }}</li>
                            </ul>
                        </td>
                        <td><a style="margin: 19px;" class="btn btn-primary"
                                href="{{ route('account.edit', $user) }}">Edit Account
                            </a>
                        </td>


                        <td style=" text-align: right;">
                            <i class="fas fa-user-circle fa-7x"></i>
                        </td>
                    </tr>
                </table>
                @if (session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
        </div>
    @endsection
