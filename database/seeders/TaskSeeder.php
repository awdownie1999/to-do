<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;
use Carbon\Carbon;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task1 = Task::firstOrNew([
            'name' => 'code a bit',
            'complete' => false,
            'type_id' => 1,
            'user_id' => 1,
            'date' => Carbon::parse('2021-01-01')
        ]);
        $task1->save();
    }
}
