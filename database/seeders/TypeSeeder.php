<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $type1 = Type::firstOrNew([
            'name' => 'Physical',
        ]);
        $type1->save();

        $type2 = Type::firstOrNew([
            'name' => 'Educational',
        ]);
        $type2->save();

        $type3 = Type::firstOrNew([
            'name' => 'Life',
        ]);
        $type3->save();

        $type4 = Type::firstOrNew([
            'name' => 'Other',
        ]);
        $type4->save();
    }
}
