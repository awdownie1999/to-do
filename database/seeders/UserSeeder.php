<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //set roles
        $test = User::firstOrNew([
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => Hash::make('password'),
        ]);
        $test->save();

        $test2 = User::firstOrNew([
            'name' => 'tester',
            'email' => 'tester@test.com',
            'password' => Hash::make('password'),
        ]);
        $test2->save();
    }
}
