<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name', 'complete', 'type_id', 'date', 'user_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
