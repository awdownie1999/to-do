<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('Account.account', compact('user'));
    }

    public function edit($user)
    {
        return view('Account.editAccount', compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $user = Auth::user();
        $user->name = $request->get('name');


        return view('Account.account', compact('msg', 'user'));
    }
}
