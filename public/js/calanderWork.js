document.addEventListener('DOMContentLoaded', function() {  
    
    var eventObjArr = [];

    var tasks = document.getElementsByClassName('tasks');
    [].forEach.call(tasks, function(task) {
        //console.log(task)
        
        let taskID = task.childNodes[0].innerText;
        let taskName = task.childNodes[2].innerText;
        let taskType = task.childNodes[4].innerText;
        let taskDate = task.childNodes[6].innerText;
        let taskComplete = task.childNodes[8].innerText;

        dateSplit = taskDate.split(' ')[0];
        //console.log(dateSplit);
        
        eventObj = {
            id : taskID,
            name : taskName,
            type : taskType,
            date : dateSplit,
            complete : taskComplete
        }
        eventObjArr.push(eventObj);
    })    
    console.log('event Object array ', eventObjArr);

    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        height: 'auto',
        aspectRatio: 1,
        initialView: 'dayGridMonth',
    });

    eventObjArr.forEach(function (eventObj){
        calendar.addEvent(eventObj);
        let event = calendar.getEventById(eventObj.id);
        event._def.title =  event._def.extendedProps.name;

        //returns string ??
        //console.log(typeof(JSON.stringify(event._def.extendedProps.type)), ' ', JSON.stringify(event._def.extendedProps.type))

        if(JSON.stringify(event._def.extendedProps.type).includes('Physical')){
            event._def.ui.backgroundColor = "cyan";
            event._def.ui.textColor = "black";

        }else if(JSON.stringify(event._def.extendedProps.type).includes('Educational')){
            event._def.ui.backgroundColor = "yellow";
            event._def.ui.textColor = "black";

        }else if(JSON.stringify(event._def.extendedProps.type).includes('Life')){
            event._def.ui.backgroundColor = "green";
            event._def.ui.textColor = "white";

        }else{
            event._def.ui.backgroundColor = "pink";
            event._def.ui.textColor = "black";
        }

        console.log(event);
    })
    if(tasks.length != 0){
        calendar.render();
    }
});
