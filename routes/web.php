<?php

use Illuminate\Support\Facades\Route;
use App\Models\Task;

Route::group(['middleware' => ['auth']], function () {
    Route::resource('tasks', 'TaskController');
    Route::resource('types', 'TypeController');
    Route::get('/account', 'AccountController@index')->name('account.index');
    Route::get('/account/{user}/edit', 'AccountController@edit')->name('account.edit');
    Route::patch('/account/update', 'AccountController@update')->name('account.update');
});
Route::get('/', 'DashController@index');


Auth::routes(['verify' => true]);
